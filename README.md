# Conceptual Design

sketch needed.

# Presentation mode

There is the presentation mode which makes the background black and removes the picture of the canvas. black is important to achieve transparency for the projection. Every color should be full color and not transparent. the level of transparency should be made physically with the beamer and the canvas.

## Mapping

- cloud color = channel name - state, region
- cloud size = count of Impressions - number inhabitants, <https://de.statista.com/statistik/daten/studie/75396/umfrage/entwicklung-der-bevoelkerung-in-oesterreich-nach-bundesland-seit-1996/>
- count of drops = count of sessions - number of infections, <https://www.data.gv.at/katalog/dataset/covid-19-anzahl-der-aktuell-erkrankten-je-bundesland/resource/f7732762-ee77-47b8-9519-90a58179b3c6>
- number of polygons = number of process start - number of hospitalized (intensivbetten) in percent, total austria, <https://www.data.gv.at/katalog/dataset/4911eb7e-ac6c-4c86-a657-8e6527d3cb8f>
- (polygon hit by drop = beat and color change)
- (polygon fall = closure as a count of collisions)
- polygon smash - well again <https://www.data.gv.at/katalog/dataset/66abba56-f867-44f1-8360-2c2a38fad42a>
- (fade out - died)

# Painting style

- 2x großer Pinsel dann kleiner werden, wild. nicht so viele sturkturen vorher
- dunkel und wenn noch nicht ganz trocken mit viel wasser und weiß drüber -scully

# Corona data

<https://www.data.gv.at/covid-19/> directly in p5.js <https://p5js.org/reference/#/p5/loadTable>

# Infrastructure

the CORS error in chrome can be prevented with this extension: <https://chrome.google.com/webstore/detail/cross-domain-cors/mjhpgnbimicffchbodmgfnemoghjakai> apply to the URLs of _localhost_ and _info.gesundheitsministerium.at_

## Notes for improvment

- check particle system for clouds
- get the brightness of the room with a sensor and adjust for it
- run smoothly, no coordination bugs

## Hollow objects

hollow objects: <https://github.com/liabru/matter-js/issues/659>

Examples:

- <https://codepen.io/danielgivens/pen/geKrRx>
- <https://codepen.io/Alca/pen/GWEWja>
- <https://codepen.io/Alca/pen/GWENyW>

# Soundtrack

Soundtrack by Alexander Krisch - <https://www.krisch-music.com> & <https://soundcloud.com/user-262680480>

- <https://soundcloud.com/user-262680480/data-painting-v1/s-j5mdlU2Zu3g>
- <https://soundcloud.com/user-262680480/data-painting-v2/s-XjvwjS2JWhS>

## Inspiration

looks interesting: <https://github.com/b-g/p5-matter-examples> bug centering fromvertices: <https://github.com/liabru/matter-js/issues/248> difference between body.position.x and centre.x

learning rotation and examples: <http://genekogan.com/code/p5js-transformations/>

### matter.js demos

- matter attractors plugin: <https://liabru.github.io/matter-attractors/#gravity>
- basic attractor <https://liabru.github.io/matter-attractors/#basic>
- time scaling: <http://brm.io/matter-js/demo/#timescale>
- terrain: <http://brm.io/matter-js/demo/#terrain> <https://github.com/liabru/matter-js/blob/master/examples/terrain.js>
- sensors: <http://brm.io/matter-js/demo/#sensors>
- events: <http://brm.io/matter-js/demo/#events>
- change gravity: <https://codepen.io/caporta/pen/EaqEoM>
- round corners: cham feature <http://brm.io/matter-js/demo/#rounded>

- fall and splatter - <http://brm.io/matter-js/demo/#compositeManipulation> ??

# Classes

## Cloud

the element needs id `cloud` and label `#cloud`.

## Drop

### drop of zones

they are identified by `circle` elements in the svg. check in file, sometimes it is called ellipse.

deep drop inspiration: <https://www.vectorstock.com/royalty-free-vector/cartoon-water-splash-with-drops-fx-vector-14677625>

## Colors

- nice tool: <https://htmlcolorcodes.com/>
- color palettes: <https://colorhunt.co/palettes/popular> e.g. <https://colorhunt.co/palette/15830>
- tetraidici und so <https://www.canva.com/colors/color-wheel/>

## Splash

resource for orientation: <https://opengameart.org/content/bloodsplatter-and-bloodsplash-animation> coding train challenge: <https://www.youtube.com/watch?v=3noMeuufLZY>

# Tech stuff

## Loglevel

using package:<https://github.com/pimterry/loglevel>

installed with

```bash
sudo npm install -g loglevel
```

imported in the html

## Creation p5.js

created with `p5 g -b "oida"` and moved the files by hand.

## Dependencies

- matter.js
- poly-decomp <https://github.com/schteppe/poly-decomp.js/>

## p5.js & matter.js

- playlist D. Shiffman <https://www.youtube.com/watch?v=urR596FsU68&list=PLRqwX-V7Uu6bLh3T_4wtrmVHOrOEM1ig_>
- repo: <https://github.com/CodingTrain/website/tree/master/Courses/natureofcode>

## matter.js

i create a matter.js body and then use the vertices method in order to draw it in p5.js.

matter.js uses decomp.js but I think it uses the quickdecomp method which does not accurately get the coordinates of the vertices. <http://brm.io/matter-js/docs/classes/Bodies.html>

## collision events

using the plugin _matter-collision-events.min.js_: <https://github.com/dxu/matter-collision-events> demo: <https://dxu.github.io/matter-collision-events/#basic>

## using Plugins

<https://github.com/liabru/matter-js/wiki/Using-plugins>

# Archive

## Shadows

you cannot draw a blur directly onto an object because it changes everything after the blur filter. push, pop do not work for filters. but you can create a specific layer with createGraphics() example: <https://alpha.editor.p5js.org/RonanChang/sketches/ByGdPoiZz>. here is a draft: <https://editor.p5js.org/sektionschef/sketches/Co7eqyTSg>

# Polygon

- am besten Polygone einsetzen statt geometrische Figuren -> <http://brm.io/matter-js/docs/classes/Bodies.html>

## Get the centroid of a polygon

I found the following snippet, here: <https://stackoverflow.com/questions/9692448/how-can-you-find-the-centroid-of-a-concave-irregular-polygon-in-javascript>

```javascript
function get_polygon_centroid(pts) {
   var first = pts[0], last = pts[pts.length-1];
   if (first.x != last.x || first.y != last.y) pts.push(first);
   var twicearea=0,
   x=0, y=0,
   nPts = pts.length,
   p1, p2, f;
   for ( var i=0, j=nPts-1 ; i<nPts ; j=i++ ) {
      p1 = pts[i]; p2 = pts[j];
      f = p1.x*p2.y - p2.x*p1.y;
      twicearea += f;          
      x += ( p1.x + p2.x ) * f;
      y += ( p1.y + p2.y ) * f;
   }
   f = twicearea * 3;
   return { x:x/f, y:y/f };
}
```

## Get area of polygon

from here: <https://stackoverflow.com/questions/16285134/calculating-polygon-area> from Andrii Verbytskyi

## create vertices from file

```javascript
polygon1 = new PolygonShape(polygon_raw_data.vexys);  // p5
polygon2 = Bodies.fromVertices(400, 300, polygon_raw_data.vexys); //matter
```

## Maptastic

for projection mapping using: <https://github.com/glowbox/maptasticjs> I defined the link to the canvas in the index.html.

```javascript
  function createMaptasticLayers() {
    var canvas_maptastic = document.getElementById("horst");
    // console.log(canvas_maptastic);  //sshould be sth like <canvas id="canvas_maptastic" width=....

    Maptastic("controls", canvas_maptastic);  // or by id - for instance "controls"
  }
  // window.addEventListener('load', createMaptasticLayers);  // called in setup function of the sketch after everything was loaded.
```

and called it in the setup function of the sketch file:

```javascript
  createMaptasticLayers();
```

# Image Normalization

In **Gimp**

- use the _Lasso_ tool to select the canvas on the photo
- save the selection to a channel
- get the only the canvas via floating layer and save it to another layer
- use the _perspective tool_ and some rulers to get orthogonal
- resize the image and export - The normalized image size should not exceed the size of the browser. e.g. height of _900px_. otherwise it will be cropped. the canvas should be matching the dimension of the image.

In **Inkscape**

- use inkscape > 1.0 to make sure the coordinates are centered around top left.
- use _absolute_ instead of _relative_ or _optimized_ coordinates in the preferences - _Input/Output_ - _SVG output_.
- File -> document properties.

  - inkscape usually uses 'mm' in Austria as unit. This can be changed in the document properties of inkscape. change _display units_ and _units_ to 'px'
  - change the scale to 1\. <https://graphicdesign.stackexchange.com/questions/118111/inkscape-can-i-use-px-in-xml-editor>
  - change image size to dimensions of canvas image. 1266x900

- import the image and set as link to keep filesize low. However, the image is resized to sth smaller. Double check the scale in the properties ro prevent zooming.

check early if the coordinates of the svg file are absolute.

- should be drawn clockwise in inkscape. otherwise errors.
- in inkscape with zigzag path option to avoid bezier curves - Shift + F6, `Create a sequence of straight line segments`.
- there is often the error for missing a pairwise coordinate. Check in inkscape at the coordinate... often there are two nodes at the same position. you can select one node and loop throuhg all of them with _Tab_ key to spot duplicates.

often you have to redraw the polygon in inkscape as long as there are only M and Z elements in the path string, no L V and shit. <https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d>

# Loading svg and create polygons

I got some issues due to async timing of the dom not beeing fully loaded when p5.js wants to look up the svg. So i loaded the svg as xml in preload section of the sketch. This way it is clear that the full svg exists for parsing the coordinates of polygons.

Then I grab the path elements, grab the id and the _d_ string. For reading the _d_ string I create an _SVGPathElement_ in order to use _pathseg_ library for getting the x, y points from the string: <https://github.com/progers/pathseg>.

## the old approach

- include in the index.html and not just display it. using ajax in index to load it from file and not copy paste the svg file in the index.html.
- loop through the path elements. these two helped me in setting things up.
- <https://stackoverflow.com/questions/25384052/convert-svg-path-d-attribute-to-a-array-of-points>
- <https://stackoverflow.com/questions/34352624/alternative-for-deprecated-svg-pathseglist>

```javascript
var shape_data = [];
var path = document.getElementById('path3713');
var path = document.getElementById('path3788');
var pathSegList = path.pathSegList;
console.log(pathSegList);
for (var i = 0; i < (pathSegList.numberOfItems -1); i++) {  // -1 last one is empty
  shape_data.push({
    'x':pathSegList.getItem(i).x,
    'y':pathSegList.getItem(i).y
  })
}
console.log(shape_data)
```

## Some vector know how

```javascript
// get the perpendicular/90 degree angle, by swapping x and y and setting one * -1
var v = createVector(h.y, -h.x);
```

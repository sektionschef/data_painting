// bubbles to fill the polygons

class Bubble {
  constructor (x, y, radius) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.hide_color = color(255, 255, 255, 0);  // transparent
    this.debug_color = color(255, 134, 255);
    // this.show_color = fill_color;
    this.remove_me = false;

    if (logging.getLevel() <= 1) {
      this.fill_color = this.debug_color;
    } else {
      this.fill_color = this.hide_color;
    }

    this.body = Bodies.circle(this.x, this.y, this.radius);

    World.add(world, this.body);
  }

  draw_bubble () {
    draw_shape_from_vertices(this.body.vertices, this.fill_color);
  }

  change_color (fill_color) {
    this.fill_color = fill_color;
  }

  destroy (dry=false) {
    if (dry == false) {
      deep_splashes.push(new Splash(deep_splash_frames, this.body.position.x, CANVAS_HEIGHT, "black"));
    }
    // this.remove_me = true;
    World.remove(world, this.body);
  }

}

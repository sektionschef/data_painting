// Clouds where the drops drop from.

class Cloud {
  constructor (polygon_data, master_data) {
    this.polygon_data = JSON.parse(JSON.stringify(polygon_data)); // original data
    this.polygon_data_sections = JSON.parse(JSON.stringify(polygon_data)); // altered with sections in it
    this.master_data = master_data;
    this.DIVIDING_Y_VALUE = 6;  // divides lines in upper and lower, used to be 20

    // lines created of the vertices above this.DIVIDING_Y_VALUE and below this.DIVIDING_Y_VALUE.
    this.lines = {
      "upper_line": [],
      "lower_line": []
    };
    this.data_per_border = {};
    this.all_drawable_sections = {};

    this.create_sections();
  }

  /** divide in upper line and lower line in respect to a manual set this.DIVIDING_Y_VALUE.

  This is important because both need to be adressed for the x axis section
  limit in order to draw a complete object.
  */
  define_upper_lower_line () {

    this.lines.upper_line = []
    this.lines.lower_line = []

    // fill the gaps with nans to keep the original index
    for (let coord in this.polygon_data_sections) {
      if (
        this.polygon_data_sections[coord].y < this.DIVIDING_Y_VALUE
      ) {
        this.lines['upper_line'].push(this.polygon_data_sections[coord])
        this.lines['lower_line'].push(NaN);
      } else {
        this.lines['upper_line'].push(NaN)
        this.lines['lower_line'].push(this.polygon_data_sections[coord]);
      };
    }
  }

  /**
  Calculate the distance between a x value on a upper or lower line
  to the section border x value.
  */
  get_distance_to_section_border() {
    var list_of_distances = [];

    logging.debug("this.current_section_border_x: " + this.current_section_border_x);

    for (let coord in this.lines[this.current_line_label]) {
      var x = this.lines[this.current_line_label][coord].x;
      // logging.debug("calc distance to x: " + x);
      var distance_x = this.current_section_border_x - x
      list_of_distances.push(distance_x)
    }
    this.data_per_border[this.current_section_border_x][this.current_line_label].list_of_distances = list_of_distances;
  }

  /**
  check the distances for the nearest point to the left and right of the
  section border's x position.
  */
  get_nearest_two_points_to_section_border (list_of_distances) {
    var list_of_distances = this.data_per_border[this.current_section_border_x][this.current_line_label].list_of_distances
    // logging.debug(list_of_distances);

    // underrunning the limit
    var list_of_distances_left = list_of_distances.filter(function(value) {
      return value >= 0
    });
    // exceeding the limit
    var list_of_distances_right = list_of_distances.filter(function(value) {
      return value <= 0
    });

    logging.debug("list_of_distances_left:")
    logging.debug(list_of_distances_left);
    logging.debug("list_of_distances_right:")
    logging.debug(list_of_distances_right);

    let i = list_of_distances.indexOf(
      Math.min.apply(null, list_of_distances_left)
    );
    let j = list_of_distances.indexOf(
      Math.max.apply(null, list_of_distances_right)
    );

    logging.debug("Found the nearest point left with point x: " + this.polygon_data_sections[i].x + " y: " + this.polygon_data_sections[i].y);
    logging.debug("Found the nearest point right with point x: " + this.polygon_data_sections[j].x + " y: " + this.polygon_data_sections[j].y);

    this.data_per_border[this.current_section_border_x][this.current_line_label].nearest_point_left_index = i
    this.data_per_border[this.current_section_border_x][this.current_line_label].nearest_point_right_index = j
    this.data_per_border[this.current_section_border_x][this.current_line_label].nearest_point_left = this.polygon_data_sections[i]
    this.data_per_border[this.current_section_border_x][this.current_line_label].nearest_point_right = this.polygon_data_sections[j]
  }

  /**
  Get the slope and the intercept of the nearest two points: y=k*x + d
  Then solve the equation for the specific x value of the section
  border to have the coordinates of a new coordinate on the cloud's outer
  line.
  */
  get_y_coordinate_on_section_border () {
    var i = this.data_per_border[this.current_section_border_x][this.current_line_label].nearest_point_left_index
    var j = this.data_per_border[this.current_section_border_x][this.current_line_label].nearest_point_right_index

    // there are two known positions on the line: y1 = k*x1 + d & y2 = k*x2 + d
    // logging.debug("getting slope between:")

    var slope_a_b = (
      this.polygon_data_sections[j].y - this.polygon_data_sections[i].y
    ) / (
      this.polygon_data_sections[j].x - this.polygon_data_sections[i].x
    )
    if (isNaN(slope_a_b)) {
      slope_a_b = 0
    };
    logging.debug("slope: " + slope_a_b);

    this.data_per_border[this.current_section_border_x][this.current_line_label].slope_between_nearest_points = Math.round((slope_a_b * 100) / 100)

    // calculate the intercept: d
    var d = this.polygon_data_sections[i].y - (slope_a_b * this.polygon_data_sections[i].x);
    this.data_per_border[this.current_section_border_x][this.current_line_label].intercept = Math.round(d);

    // what is the y value for the known x value of the section border
    this.current_section_border_y = Math.round(
      (slope_a_b * this.current_section_border_x) + d
    );

    logging.debug("For the section border of " + this.current_section_border_x + " on the " + this.current_line_label + " the y value of " + this.current_section_border_y + " was calculated.")

    this.data_per_border[this.current_section_border_x][this.current_line_label].y = this.current_section_border_y;
    this.data_per_border[this.current_section_border_x][this.current_line_label].x = this.current_section_border_x;
  }

  /*
  insert the newly found coordinates to the array of vertices.
  The order in the array is dependent on upper or lower line.
  */
  insert_section_coords () {
    if (this.current_line_label == "upper_line") {
      var index_to_be = this.data_per_border[this.current_section_border_x][this.current_line_label].nearest_point_right_index;
    } else if (this.current_line_label == "lower_line") {
      var index_to_be = this.data_per_border[this.current_section_border_x][this.current_line_label].nearest_point_left_index;
    } else {
      throw "Oh boy, no upper or lower line."
    }

    var x = this.data_per_border[this.current_section_border_x][this.current_line_label].x
    var y = this.data_per_border[this.current_section_border_x][this.current_line_label].y

    logging.debug("adding section coords: x: " + x + " y: " + y);

    // check if the coordinate is already present
    for (var coord of this.polygon_data_sections) {
      if (
        (coord.x == x) & (coord.y == y)
      ) {
        // logging.debug("Coordinate is already there. It is skipped.");
        return
      }
    }
    this.polygon_data_sections.splice(index_to_be, 0, {"x": x, "y": y});
  }

  /**Split in sections of vertices to draw them in different colors.*/
  get_drawable_sections () {
    for (let section in this.master_data) {
      var section_vertices = []
      for (let coord in this.polygon_data_sections) {
        if (this.polygon_data_sections[coord].x >= this.master_data[section].section_borders[0] & this.polygon_data_sections[coord].x <= this.master_data[section].section_borders[1]) {
          section_vertices.push(this.polygon_data_sections[coord]);
        }
      }
      // section_vertices = sort_vertices_clockwise(section_vertices);  // not needed
      this.all_drawable_sections[section] = section_vertices
    }
  }

  /** create and define the sections for drawing them in a loop.*/
  create_sections () {
    logging.debug("polygon data sections:")
    logging.debug(this.polygon_data_sections)

    for (let section in this.master_data) {
      for (let section_limit of this.master_data[section].section_borders) {
        this.current_section_border_x = section_limit;
        this.data_per_border[this.current_section_border_x] = {}

        for (let line in this.lines) {
          this.current_line_label = line;
          this.data_per_border[this.current_section_border_x][this.current_line_label] = {}
          this.define_upper_lower_line();
          this.get_distance_to_section_border();
          this.get_nearest_two_points_to_section_border();
          this.get_y_coordinate_on_section_border();
          this.insert_section_coords();
        }
      }
    }
    logging.debug("upper and lower lines:")
    logging.debug(this.lines);
    logging.debug("data per border:")
    logging.debug(this.data_per_border);

    this.get_drawable_sections();
    logging.debug("drawable sections:")
    logging.debug(this.all_drawable_sections);
  }

  draw() {
    if (logging.getLevel() <= 1) {
      let fill_color = false;
      draw_shape_from_vertices(this.polygon_data, fill_color, "black");
    }
  }

  draw_points () {
    if (logging.getLevel() <= 1) {
      for (let section_limits in this.master_data) {
        for (let section_limit of this.master_data[section_limits].section_borders) {
          for (let line in this.lines) {
            push();
            strokeWeight(4);
            stroke(0, 0, 0);
            // console.log(this.data_per_border[section_limit][line].nearest_point_left.x);
            point(this.data_per_border[section_limit][line].nearest_point_left.x, this.data_per_border[section_limit][line].nearest_point_left.y);
            point(this.data_per_border[section_limit][line].nearest_point_right.x, this.data_per_border[section_limit][line].nearest_point_right.y);
            pop();

            push();
            strokeWeight(8);
            stroke(0, 0, 0);
            point(this.data_per_border[section_limit][line].x, this.data_per_border[section_limit][line].y);
            pop();
          }
        }
      }
    }
  }

  draw_section() {
    for (let section in master_data) {
          draw_shape_from_vertices(this.all_drawable_sections[section], this.master_data[section].color);
      // }
    }
  }
}

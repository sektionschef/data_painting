/*
Drop comes from some channel cloud and represents sessions.
create a physical body twin with the most important vertices
without bezier curves since they dont work in matter.js

the drops should not hang too long, when the next drop is formed and the prior
drop is still hanging they collide and will get removed.
*/

class Drop {
  constructor(x, y, fill_color) {
    this.height = 16;
    this.frame_delay_forming = 2;
    this.time_for_hanging = 200; // in milliseconds
    // this.attractive_body_color = color(255, 0, 255, 255);
    this.attractive_body_color = fill_color;

    this.x = x;
    this.y = y - this.height;
    this.x_origin = x;
    this.y_origin = y - this.height;
    this.collided = false;  // flag for for collision state

    this.forming_limit_y = (this.y_origin + this.height/4);  // the point were it stops to form and just hangs around and waits.

    this.construct_physical_body();
  }

  construct_physical_body() {
    this.body = Bodies.polygon(this.x, this.y, 3, this.height/2, {  // x, y, how many sides, length of side
        chamfer: {  // round the edges
          radius: [
            this.height/3,
            this.height/3,
            0
          ]
        },
        angle: -0.523598776, //-30 degrees
        position: {
          x: (this.x),
          y: (this.y + this.height/2), // offset centroid versus top left
        },
        isStatic: true  // for debugging
    });
    // console.log(this.body);  // for debugging

    World.add(world, this.body);
  }

  /*
  Bezier curves https://p5js.org/reference/#/p5/bezierVertex with x2 and x3. get the coordinates relative to the height.
  Bezier curve editor: https://www.desmos.com/calculator/cahqdxeshd
  */
  draw_attractive_body() {
    let bezier_x2 = this.height/2/2;
    let bezier_x3 = this.height/2;
    let bezier_y2 = this.height*2/3;
    let bezier_y3 = this.height*3/4;

    fill(this.attractive_body_color);
    beginShape();
  	vertex(this.x, this.y);
  	bezierVertex(this.x + bezier_x2, this.y + bezier_y2, this.x + bezier_x3, this.y + bezier_y3, this.x, this.y + this.height);
    vertex(this.x, this.y + this.height);
    bezierVertex(this.x - bezier_x3, this.y + bezier_y3, this.x - bezier_x2, this.y + bezier_y2, this.x, this.y);
  	endShape();
  }

  draw_physical_body() {
    var physical_body_color = color(255, 0, 0, 70);
    draw_shape_from_vertices(this.body.vertices, physical_body_color);
  }

  draw_masking_object() {
    push();
    fill(background_color);
    noStroke();
    if (logging.getLevel() <= 1) {
      stroke(0);
    }
    rect(this.x_origin - this.height/2, this.y_origin, this.height, this.height);
    pop();
  }

  draw_debugging_coords() {
    push();
    stroke(0);
    strokeWeight(2);
    point(this.x, this.y);
    pop();
  }

  first_phase_of_forming() {
      if (frameCount % this.frame_delay_forming == 0 && this.y <= this.forming_limit_y) {
        this.y += 1;
        Body.translate(this.body, {x: 0, y: 1});  // add +1 for the physical body
      }
      // logging.debug(this.y);  // debug
      // console.log((this.y_origin + this.height/4));  // debug
  }

  fall() {
    Body.setStatic(this.body, false); // initiates gravity

    // update the position of the instance to sync physical body twin with attractive body twin.
    this.x = this.body.position.x;
    this.y = this.body.position.y - this.height/2;
  }

  second_phase_of_falling() {
      if (this.y >= this.forming_limit_y) {
        setTimeout(this.fall.bind(this), this.time_for_hanging);
      }
    }

  splash_and_burst() {
    splashes.push(new Splash(splash_frames, this.body.position.x, this.body.position.y, this.attractive_body_color));
    var number_of_bodies_before = world.bodies.length
    World.remove(world, this.body);
    var number_of_bodies_after = world.bodies.length
    // logging.debug("Body count in physics world removed from " + str(number_of_bodies_before) + " to " + str(number_of_bodies_after) + ".");
    this.collided = true;
  }

  third_phase_of_splash_and_burst() {
    if (this.y > 50) {  // prevent from bursting in the cloud.
      this.body.onCollide(this.splash_and_burst.bind(this));
    }
  }

  pass_lifecycle() {
    if (logging.getLevel() <= 1) {
      this.draw_physical_body();
    }

    this.draw_attractive_body();
    if (logging.getLevel() <= 1) {
      this.draw_masking_object();
    }

    if (logging.getLevel() <= 1) {
      this.draw_debugging_coords();
    }

    this.first_phase_of_forming();
    this.second_phase_of_falling();
    this.third_phase_of_splash_and_burst();
  }
}

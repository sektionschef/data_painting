/*
painted objects on the canvas.
Polygonshape represents number of process starts.
*/

// left one not physical body

class PolygonShape {
  constructor(coordinates, label) {
    this.label = label;  // id of svg path for easier identification
		this.coordinates = JSON.parse(JSON.stringify(coordinates));  // original values
		this.coordinates_boomed = JSON.parse(JSON.stringify(coordinates));  // dynamic values
    // this.coordinates_physical_frame = JSON.parse(JSON.stringify(coordinates));  // for frame

    this.centroid = get_polygon_centroid(this.coordinates);
    this.area = get_area(this.coordinates);
    this.brick_bodies = [];
    this.bubbles = [];

    this.width_frame = 10;
    this.radius_of_bubbles = 5;
    // this.fill_color = color(140);
    this.fill_color = COLORS_USED[int(random(0, 3))];
    this.active_attractive_body = false;  // should it be drawn - active_attractive_body
    this.active_bubbles = false;  // active_bubbles

    logging.debug("Creating polygon: " + str(this.label));
    // console.log(this.coordinates);
    // console.log("centroid.x: " + this.centroid.x);
    // console.log("centroid.y: " + this.centroid.y);
    // console.log("area: " + this.area);

    this.calc_bubbles_for_area()
    this.create_physical_polygon_body();
    // console.log(this.body.vertices);
    this.coordinates_physical = this.body.vertices;
    this.create_physical_outer_frameborder();

    this.blur_level_start = 1;
    this.blur_level_stop = 10;
    this.speed_shadow = 10;

    this.shadow_layer = createGraphics(CANVAS_WIDTH, CANVAS_HEIGHT);
    this.blur_level = this.blur_level_start;
    this.create_shadows_once();  // takes really long;
  }

  activate() {
    this.active_attractive_body = true;
    this.active_bubbles = true;
    World.add(world, this.body);
  }

  // CLASS METHODS?
  static get_indexes_of_active_inactive_polygons () {
    PolygonShape.polygon_shape_active_count = 0;

    PolygonShape.polygon_shape_actives = [];
    PolygonShape.polygon_shape_inactives = [];

    for (var i = 0; i < polygon_shapes.length; i++) {
       if (polygon_shapes[i].active_attractive_body == true) {
         PolygonShape.polygon_shape_active_count += 1;
         PolygonShape.polygon_shape_actives.push(i);
       } else {
         PolygonShape.polygon_shape_inactives.push(i);
       }
    }
    // logging.debug("active polygon shapes " + PolygonShape.polygon_shape_active_count + " of " + polygon_shapes.length)
  }

  // get the polygons some more time for drawing their bubbles
  static get_polygon_bubble_afterlife () {
    PolygonShape.polygon_shape_bubbles_survive_count = 0;

    PolygonShape.polygon_shape_bubbles_survive = [];

    for (var i = 0; i < polygon_shapes.length; i++) {
      if (polygon_shapes[i].active_bubbles == true) {
        PolygonShape.polygon_shape_bubbles_survive_count += 1;
        PolygonShape.polygon_shape_bubbles_survive.push(i);
      }
    }
  }

  static activate_random_inactive_polygon () {
    if (
      PolygonShape.polygon_shape_active_count < active_polygon_shape_count &
      frameCount % PolygonShape.POLYGON_COMEBACK_PULSE == 0
    ) {
      let polygon_shape_index = PolygonShape.polygon_shape_inactives[
        int(random(PolygonShape.polygon_shape_inactives.length))
      ];
      polygon_shapes[polygon_shape_index].activate();
    }
  }

  static draw_active_polygon_shapes () {
    for (let active of PolygonShape.polygon_shape_actives) {
      polygon_shapes[active].draw_attractive_body();
      polygon_shapes[active].draw_physical_body()
      polygon_shapes[active].draw_physical_outer_frameborder();
      polygon_shapes[active].draw_physical_bricks();

      if (logging.getLevel() <= 1) {
        stroke(255);
        point(polygon_shapes[active].centroid.x, polygon_shapes[active].centroid.y);
        point(polygon_shapes[active].physical_centre.x, polygon_shapes[active].physical_centre.y);
        point(polygon_shapes[active].body.position.x, polygon_shapes[active].body.position.y)
      }
    }
  }

  static remove_polygon_shape_by_event () {
    if (puff_polygon_event == true) {
      // polygon1.puff_and_remove();
      if (PolygonShape.polygon_shape_actives.length > 0) {
        var polygon_shape_index = PolygonShape.polygon_shape_actives[int(random(PolygonShape.polygon_shape_actives.length))];
        polygon_shapes[polygon_shape_index].puff_and_remove();
      }
    }
  }

  // for the drops to hit
  create_physical_polygon_body () {
    this.body = Bodies.fromVertices(
      this.centroid.x,
      this.centroid.y,
      this.coordinates, {
        isStatic: true
    })

    // no idea why there is a difference between position of body and the centre of vertices.
    this.physical_centre = Matter.Vertices.centre(this.body.vertices);

    // correct for differences in centers.
    var offset_x = this.centroid.x - this.physical_centre.x;
    var offset_y = this.centroid.y - this.physical_centre.y;

    Matter.Body.setPosition(this.body, {
      x: this.centroid.x + offset_x,
      y: this.centroid.y + offset_y
    });

    // there are three different centers
    // console.log(this.centroid.x);  // calculated original polygon
    // console.log(this.body.position.x);  // position of matter.js shape
    // console.log(this.physical_centre.x);  // calculated by matter.js vertices
  }

  // how many bubbles for the area of the polygon
  calc_bubbles_for_area () {
    if (isNaN(this.area)) {
      this.bubble_fill_count = 15;
    } else {
      this.bubble_fill_count = Math.floor(this.area / Math.pow(this.radius_of_bubbles * 2, 2));
    }
  }

	// boom_color_change() {
		// if (boom_event_expand == true) {
			// this.fill_color = color( random(0,255), random(0,255), random(0,255) );
		// }
	// }

	// should it be added or subracted, relative to centroid - // in which direction to expand from the center.
	get_polarity_for_vertices_expansion(point_dimension, centroid_dimension) {
		var polarity = 1

		if (point_dimension < centroid_dimension) {  // in which direction to expand from the center.
				polarity = -1;
		}

    if (point_dimension == centroid_dimension) {  // if no difference than no change for this dimension, for instance just add to y not to x.
        polarity = 0;
    }
		return polarity
	}

  // make the polygon bigger for a beat
	// boom_expand() {
	// 	if (boom_event_expand == true) {
  //
	// 		var boom_boost = 5;
  //
	// 		for (var coord in this.coordinates_boomed) {
  //
	// 			var polarity_x = this.get_polarity_for_vertices_expansion(this.coordinates_boomed[coord].x, this.body.position.x)
	// 			var polarity_y = this.get_polarity_for_vertices_expansion(this.coordinates_boomed[coord].y, this.body.position.y)
  //
	// 			this.coordinates_boomed[coord].x += boom_boost * polarity_x;
	// 			this.coordinates_boomed[coord].y += boom_boost * polarity_y;
	// 		}
	// 	}
	// }

	// reduce the boom effect with some decay
	boom_expand_decay() {
		var frame_delay = 5; // how fast is a boom_expand resolved and the polygon returns to the original coordinates.
		var boom_deboost = 1;

		if (frameCount % frame_delay == 0) {
			for (var coord in this.coordinates_boomed) {
				if (this.coordinates_boomed[coord].x < this.coordinates[coord].x) {
					this.coordinates_boomed[coord].x += boom_deboost;
				} else if (this.coordinates_boomed[coord].x > this.coordinates[coord].x) {
					this.coordinates_boomed[coord].x -= boom_deboost;
				}
				if (this.coordinates_boomed[coord].y < this.coordinates[coord].y) {
					this.coordinates_boomed[coord].y += boom_deboost;
				} else if (this.coordinates_boomed[coord].y > this.coordinates[coord].y) {
					this.coordinates_boomed[coord].y -= boom_deboost;
				}
			}
		}
	}

  // create a hollow static object for matter engine, for including bubbles in it
  create_physical_outer_frameborder() {
    this.coordinates_physical_frame = [];

		for (var coord in this.coordinates_physical) {
      this.coordinates_physical_frame[coord] = {};

			var polarity_x = this.get_polarity_for_vertices_expansion(this.coordinates_physical[coord].x, this.centroid.x)
			var polarity_y = this.get_polarity_for_vertices_expansion(this.coordinates_physical[coord].y, this.centroid.y)

			this.coordinates_physical_frame[coord].x = this.coordinates_physical[coord].x + this.width_frame * polarity_x;
			this.coordinates_physical_frame[coord].y = this.coordinates_physical[coord].y + this.width_frame * polarity_y;
		}
  }

  draw_physical_body () {
    if (logging.getLevel() <= 1) {
      draw_shape_from_vertices(this.body.vertices, color(255, 0, 0, 70), color(255, 0, 0, 150));
    }
  }

  draw_physical_outer_frameborder() {
    if (logging.getLevel() <= 1) {
      draw_shape_from_vertices(this.coordinates_physical_frame, false, color(255, 0, 0));
    }
  }

	draw_attractive_body() {
    if (this.active_attractive_body == true) {
  		// this.boom_color_change();
  		// this.boom_expand();
      this.draw_shadow();
      draw_shape_from_vertices(this.coordinates_boomed, this.fill_color)
  		// this.boom_expand_decay();
    }
	}

  // sepp
  draw_the_bubbles() {
    if (this.active_bubbles == true) {
      for (var i = 0; i < this.bubbles.length; i++) {;
        if (this.bubbles[i].body.position.y > (CANVAS_HEIGHT - 20)) {
          var number_of_bodies_before = world.bodies.length
          this.bubbles[i].destroy();
          // this.bubbles[i].body.onCollide(this.bubbles[i].destroy.bind(this));  // bind(this) does not work well
          var number_of_bodies_after = world.bodies.length
          logging.debug("Body count in physics world removed from " + str(number_of_bodies_before) + " to " + str(number_of_bodies_after) + ".");
          this.bubbles.splice(i, 1);
          i--;
        } else {
          this.bubbles[i].draw_bubble();
        };
      }
    }
  }

  /*
  I need to create physical bricks for each side to fully enclose a polygon with matter.
  coordinate 1: start of original line
  coordinate 2: start of physical line
  coordinate 3: end of physical line
  coordinate 4: end of original line
  */
  create_physical_border_bricks_coordinates() {
    this.brick_coords = {};

    for (var coord = 0; coord < this.coordinates_physical.length; coord++) {
      let brick = "brick_" + str(coord);
      this.brick_coords[brick] = [];

      let x_origin_1 = this.coordinates_physical[coord].x
      let y_origin_1 = this.coordinates_physical[coord].y

      this.brick_coords[brick].push({
        "x": x_origin_1,
        "y": y_origin_1
      });

      let x_frame_1 = this.coordinates_physical_frame[coord].x
      let y_frame_1 = this.coordinates_physical_frame[coord].y

      this.brick_coords[brick].push({
        "x": x_frame_1,
        "y": y_frame_1
      });

      // if the last item then use the first one as the next
      let next_one_index = coord+1;
      if (next_one_index == this.coordinates_physical.length) {
        next_one_index = 0;
      }

      let x_frame_2 = this.coordinates_physical_frame[next_one_index].x
      let y_frame_2 = this.coordinates_physical_frame[next_one_index].y

      this.brick_coords[brick].push({
        "x": x_frame_2,
        "y": y_frame_2
      });

      let x_origin_2 = this.coordinates_physical[next_one_index].x
      let y_origin_2 = this.coordinates_physical[next_one_index].y

      this.brick_coords[brick].push({
        "x": x_origin_2,
        "y": y_origin_2
      });
    }
  }

  create_physical_border_bricks() {
    logging.debug("create pyhsical border bricks: " + this.label);
    this.create_physical_border_bricks_coordinates();

    for (var coord = 0; coord < this.coordinates_physical.length; coord++) {
      var body_coords = this.brick_coords["brick_" + str(coord)];
      // console.log(body_coords);

      var body = Bodies.fromVertices(0, 0, body_coords, {
        isStatic: true
      })

      // get the points which are on the inner ring/physical body vertices to calculate the midway point
      let points_of_physical_body_index = [];
      for (var i=0; i < body_coords.length; i++) {
        logging.debug(body_coords[i]);
        for (var single_coordinate of this.coordinates_physical) {
          logging.debug(single_coordinate);
          if (single_coordinate.x == body_coords[i].x & single_coordinate.y == body_coords[i].y) {
            logging.debug("Found point in phyiscal body");
            points_of_physical_body_index.push(i);
          }
        }
      }

      // error handling
      if (points_of_physical_body_index.length != 2) {
        logging.info("polygon: " + this.label);
        logging.info("this.coordinates: ");
        logging.info(this.coordinates);
        logging.info("this.body.vertices: ");
        logging.info(this.body.vertices);
        logging.info("body_coords: ");
        logging.info(body_coords);
        logging.info("this.coordinates_physical: ");
        logging.info(this.coordinates_physical);
        logging.info("\npoints_of_physical_body_index: ");
        logging.info(points_of_physical_body_index);
        logging.info(body_coords[points_of_physical_body_index[0]].x);
        logging.info(body_coords[points_of_physical_body_index[0]].y);
        logging.info(body_coords[points_of_physical_body_index[1]].x);
        logging.info(body_coords[points_of_physical_body_index[1]].y);
        throw "There should be only two points on the outline of the physical bodies.";
      }

      let vector_a = createVector(body_coords[points_of_physical_body_index[0]].x, body_coords[points_of_physical_body_index[0]].y)
      let vector_b = createVector(body_coords[points_of_physical_body_index[1]].x, body_coords[points_of_physical_body_index[1]].y)

      // vectors will get overwritten
      var a = vector_a;
      var b = vector_b;

      // get the position half way between a and b on the inner ring / the physical body
      a.sub(b).div(2);
      var point_midway = a.add(b);
      this.point_midway = point_midway.copy();

      // create vector between point midway and the centroid
      var g = createVector(point_midway.x - this.centroid.x, point_midway.y - this.centroid.y)
      var angle = g.heading();
      var n = p5.Vector.fromAngle(angle, this.width_frame/2);
      n.add(point_midway);
      this.n = n

      Matter.Body.setPosition(body, {
        x: n.x,
        y: n.y
      });

      this.brick_bodies.push(body);
    }

    World.add(world, this.brick_bodies);
  }

  draw_physical_bricks() {
    if (logging.getLevel() <= 1) {
      for (var brick of this.brick_bodies) {
        draw_shape_from_vertices(brick.vertices, color(200), color(100));
        // draw the angle
        stroke('black');
        strokeWeight(1);
        line(this.point_midway.x, this.point_midway.y, this.n.x, this.n.y);
        // midway point
        ellipse(this.point_midway.x, this.point_midway.y, 3);
      }
    }
  }

  fill_with_bubbles() {
    for (var i=0; i < this.bubble_fill_count; i++) {
      this.bubbles.push(new Bubble(this.centroid.x, this.centroid.y, this.radius_of_bubbles));
    }
  }

  // make them visible
  change_bubbles_color() {
    for (let bubble of this.bubbles) {
        bubble.change_color(this.fill_color);
    }
  }


  destroy_all_bubbles () {
    var number_of_bodies_before = world.bodies.length
    for (let bubble of this.bubbles) {
      bubble.destroy(dry=true);
    }
    var number_of_bodies_after = world.bodies.length
    // logging.debug("Body count in physics world removed from " + str(number_of_bodies_before) + " to " + str(number_of_bodies_after) + ".");
    this.bubbles = [];
    this.active_bubbles = false;
  }


  remove_objects_and_cleanup () {
    var number_of_bodies_before = world.bodies.length
    // alert("DEBUG Hammer");
    World.remove(world, this.brick_bodies);
    var number_of_bodies_after = world.bodies.length
    // logging.debug("Body count in physics world removed from " + str(number_of_bodies_before) + " to " + str(number_of_bodies_after) + ".");
    this.brick_bodies = [];
    this.active_attractive_body = false;  // do not draw anymore

    this.change_bubbles_color();
    setTimeout(this.destroy_all_bubbles.bind(this), 4000);  // if some are stuck on another polygon
  }

  puff_and_remove() {
    World.remove(world, this.body);  // still visible in sketch but removed from world
    this.create_physical_border_bricks();
    setTimeout(this.fill_with_bubbles.bind(this), 300);

    setTimeout(this.remove_objects_and_cleanup.bind(this), 1000)
  }

  // so slow so make this static, planned: loop from blur_level_start to blur_level_stop
  create_shadows_once() {
    // alternative for dynamic growing of shadow.
    // if (frameCount % this.speed_shadow == 0 && this.blur_level <= this.blur_level_stop) {
    this.shadow_layer.clear();

    this.shadow_layer.push();
    this.shadow_layer.beginShape();
    this.shadow_layer.fill("black");
    for (var i = 0; i < this.coordinates_boomed.length; i++) {
      this.shadow_layer.vertex((this.coordinates_boomed[i].x), (this.coordinates_boomed[i].y));
    }
    this.shadow_layer.endShape(CLOSE);
    this.shadow_layer.pop();

    this.shadow_layer.filter(BLUR, this.blur_level_stop);
      // this.blur_level += 1;

      // console.log("blur level: " + this.blur_level);
    }

  draw_shadow() {
    image(this.shadow_layer, 0, 0);
  }
}

PolygonShape.POLYGON_COMEBACK_PULSE = 200;

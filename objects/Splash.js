// splash for drops - the grande finale

class Splash {
  constructor (frames, x, y, color) {
    this.frames = frames;
    this.x = x;
    this.y = y;
    this.image_redux_size = 25;

    if (logging.getLevel() <= 1) {
      this.color = "black";
    } else {
      this.color = color;
    }

    this.frame_index = 0;
    this.speed = 2;  // speed of animation
    this.remove_me = false;  // flag for finished animations -> take off screen
  }

  show() {
      push();
      tint(this.color);

      // correct position for image size
      this.transform_x = this.x - (this.image_redux_size/2)
      this.transform_y = this.y - this.image_redux_size

      image(this.frames[this.frame_index], this.transform_x, this.transform_y, this.image_redux_size, this.image_redux_size);

      // frame count
      if (frameCount % this.speed == 0) {
        this.frame_index += 1;
      }
      if (this.frame_index >= this.frames.length) {
        this.remove_me = true;
      }
      pop();
    }
}

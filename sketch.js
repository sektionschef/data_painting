// SWITHCES
let SWITCH_PRESENTATION_MODE = true;
// let SWITCH_PRESENTATION_MODE = false;

// let SWITCH_LOGGING_LEVEL = "debug"  // trace, debug, info, warn, error
let SWITCH_LOGGING_LEVEL = "info"  // trace, debug, info, warn, error

// use the corona data
// let SWITCH_REALTIME_DATA = true;
let SWITCH_REALTIME_DATA = false;

// mind aspect ratio of image
// let max_CANVAS_WIDTH = 1400;
// let max_CANVAS_HEIGHT = 900;
let CANVAS_WIDTH = 1266;
let CANVAS_HEIGHT = 900;

let background_color = 110;  // 0 is no light.

let painting_image;
// let polygon_raw_data;
let polygon_data = {};
let drop_off_zone_data = {};
let cloud;
let polygon1;
let drops = [];
let bubbles = [];
let polygon_shapes = [];
let active_polygon_shape_count;
let xml;
let splash_frames;
let splashes = [];
let deep_splash_frames;
let deep_splashes = [];


// colors: triadic picked from the blue of the canvas - https://htmlcolorcodes.com/
// beat: between fast and slow: [10, 800]
let master_data = {
  "vienna": {
    "data_label": "Wien",
    "color": '#94ADC1FF',
    "number_of_inhabitants": 1911191,
    // "number_of_infections": 497,
    "number_of_recovered": 6725,
    // "section_borders": [0, 500],
    "beat": 100,  // 374
  },
  "lower_austria": {
    "data_label": "Niederösterreich",
    "color": '#C4AEBFFF',
    "number_of_inhabitants": 1684287,
    // "number_of_infections": 322,
    "number_of_recovered": 3772,
    // "section_borders": [500, 900],
    "beat": 200,
  },
  "upper_austria": {
    "data_label": "Oberösterreich",
    "color": "#A8A690FF",
    "number_of_inhabitants": 1490279,
    // "number_of_infections": 1886,
    "number_of_recovered": 4438,
    // "section_borders": [900, CANVAS_WIDTH],
    "beat": 300,
  }
}

master_data_total = {
  "active_polygons": 10,
  // "percent_beds_intensive": 4,
  // "total_inhabitants": 5085757,
  // "total_recovered": 14935,
  "polygon_puff": 636,  // 636 ?
}


// matter.js stuff
var Engine = Matter.Engine;
var World = Matter.World;
var Body = Matter.Body;
var Bodies = Matter.Bodies;
var Composite = Matter.Composite;

Matter.use('matter-collision-events');

var engine;
var world;
var ground;

let table_infections;
let table_beds_intensive;
let table_recovered;

let COLORS_USED = [];
for (let section in master_data) {
  COLORS_USED.push(master_data[section]["color"])
}

function preload() {
  table_infections = loadTable('https://info.gesundheitsministerium.at/data/Bundesland.csv', 'ssv', 'header');
  table_beds_intensive = loadTable('https://info.gesundheitsministerium.at/data/IBAuslastung.csv', 'ssv', 'header');
  table_recovered = loadTable('https://info.gesundheitsministerium.at/data/GenesenTodesFaelleBL.csv', 'ssv', 'header');

  // polygon_raw_data = loadJSON("coordinates_polygons.json");
  xml = loadXML('images/polygon_mapping_master.svg');
  painting_image = loadImage("images/sneyder_normalized.png");

  // producing CORS error

  // the image frames for the drop splashes.
  splash_frames = [];
  splash_frames[0] = loadImage('images/splash/splash_custom_0.png');
  splash_frames[1] = loadImage('images/splash/splash_custom_1.png');
  splash_frames[2] = loadImage('images/splash/splash_custom_2.png');
  splash_frames[3] = loadImage('images/splash/splash_custom_3.png');
  splash_frames[4] = loadImage('images/splash/splash_custom_4.png');

  // the image frames for the drop splashes.
  deep_splash_frames = [];
  deep_splash_frames[0] = loadImage('images/deep_splash/deep_splash_0.png');
  deep_splash_frames[1] = loadImage('images/deep_splash/deep_splash_1.png');
  deep_splash_frames[2] = loadImage('images/deep_splash/deep_splash_2.png');
  deep_splash_frames[3] = loadImage('images/deep_splash/deep_splash_3.png');
  deep_splash_frames[4] = loadImage('images/deep_splash/deep_splash_4.png');
  deep_splash_frames[5] = loadImage('images/deep_splash/deep_splash_5.png');
  deep_splash_frames[6] = loadImage('images/deep_splash/deep_splash_6.png');
  deep_splash_frames[7] = loadImage('images/deep_splash/deep_splash_7.png');
}

function setup() {
  logging.setLevel(SWITCH_LOGGING_LEVEL);

	var canvas = createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
  canvas.id("canvas_element");
  engine = Engine.create();
  world = engine.world;

  var ground_height = 60;
  ground = Bodies.rectangle(CANVAS_WIDTH/2, (CANVAS_HEIGHT + ground_height/2), (CANVAS_WIDTH + 100), ground_height, { // mind center mode
    isStatic: true
  });

  read_table_infections();
  read_table_beds();
  read_table_recovered();

  calc_master_data_inhabitants();

  get_data_from_polygon_mapping_svg();

  create_cloud_and_polygons();
  allocate_drop_off_zone_to_section();

  // how many polygons to draw
  // active_polygon_shape_count = 25;
  active_polygon_shape_count = master_data_total["active_polygons"];

  // debug polygon
  // polygon1 = new PolygonShape(polygon_raw_data.vexys);  // custom class
  // polygon2 = Bodies.fromVertices(400, 300, polygon_raw_data.vexys);  // directly from raw data


  World.add(world, [ground]);
  Engine.run(engine);
  createMaptasticLayers();

  logging.info("master data:");
  logging.info(master_data);

  logging.info("master data total:");
  logging.info(master_data_total);
}

function draw() {
  // console.log("mouseX: " + str(mouseX));
  // console.log("mouseY: " + str(mouseY));

  Engine.update(engine);

  // boom_event_expand = createBeat(60);

  // ATTENTION - Data or fixed
  // puff_polygon_event = createBeat(600);
  puff_polygon_event = createBeat(master_data_total["polygon_puff"]);

  PolygonShape.get_indexes_of_active_inactive_polygons();
  PolygonShape.get_polygon_bubble_afterlife();
  PolygonShape.activate_random_inactive_polygon();

  if (SWITCH_PRESENTATION_MODE) {
    background(background_color);
  } else {
    background(painting_image, 10);
  }
  noStroke();

  for (var drop_off_zone in drop_off_zone_data) {
    if (createBeat(drop_off_zone_data[drop_off_zone].beat)) {
      drops.push(new Drop(
        drop_off_zone_data[drop_off_zone].x,
        drop_off_zone_data[drop_off_zone].y,
        drop_off_zone_data[drop_off_zone].color,
      ));
    }
  }

  for (let drop of drops) {
    drop.pass_lifecycle();
  }

  // draw_shape_from_vertices(polygon2.vertices, color(20));
  draw_shape_from_vertices(ground.vertices, color(127));

  cloud.draw_section();
  cloud.draw();
  cloud.draw_points();

  PolygonShape.draw_active_polygon_shapes();

  for (let bubbly of PolygonShape.polygon_shape_bubbles_survive) {
    polygon_shapes[bubbly].draw_the_bubbles();
  }

  // polygon1.draw_attractive_body();
  // polygon1.draw_physical_outer_frameborder();
  // polygon1.draw_physical_bricks();

  PolygonShape.remove_polygon_shape_by_event()
  remove_bodies_from_screen(drops);


  for (let splash of splashes) {
    splash.show();
  }
  // cleanup
  for (var i = 0; i < splashes.length; i++) {;
    if (splashes[i].remove_me) {
      splashes.splice(i, 1);
      i--;
    }
  }

  for (let deep_splash of deep_splashes) {
    deep_splash.show();
  }

  // cleanup
  for (var i = 0; i < deep_splashes.length; i++) {;
    if (deep_splashes[i].remove_me) {
      deep_splashes.splice(i, 1);
      i--;
    }
  }
}

function createBeat(pulse_in_seconds) {
  if (frameCount % pulse_in_seconds == 0) {
    return true;
  }
  else {
    return false;
  }
}

function draw_shape_from_vertices(vertices, fill_color = false, stroke_color = false) {
  push();
  beginShape();
  if (fill_color == false) {
    noFill();
  } else {
    fill(fill_color);
  }
  if (stroke_color == false) {
    noStroke();
  } else {
    stroke(stroke_color);
  }
  for (var i = 0; i < vertices.length; i++) {
    vertex((vertices[i].x), (vertices[i].y));  // correct for translate usage, and fiddling around to match draw mode of left top (p5.js) to center (matter.js)
  }
  endShape(CLOSE);
  pop();
}

// general function for removing bodies from an array
function remove_bodies_from_screen(bodies) {
  for (var i = 0; i < bodies.length; i++) {;
    if (bodies[i].collided) {
      bodies.splice(i, 1);
      i--;
    }
  }
}

// load the data from svg and put them in a JSON with coordinates per shape
function get_data_from_polygon_mapping_svg() {

  let g = xml.getChild('g');  // get the first one
  logging.debug("raw data xml:");
  logging.debug(g);
  // print(g.DOM.children.length);
  // console.log(g.DOM.children)

  for (let i = 0; i < g.DOM.children.length; i++) {
    // console.log(g.DOM.children[i].localName);
    if (g.DOM.children[i].localName == "image") {
      continue;
    } else if (g.DOM.children[i].localName == "circle") {
      get_drop_off_zones_svg(g.DOM.children[i]);
    } else if (g.DOM.children[i].localName == "path") {
      get_polygon_data_svg (g.DOM.children[i]);
    }
  }
  logging.debug("polygon_data:")
  logging.debug(polygon_data);
  logging.debug("drop_off_zone_data:")
  logging.debug(drop_off_zone_data);

}

function get_drop_off_zones_svg (child) {
  var coords = {};

  coords['x'] = int(child.attributes.cx.value);
  coords['y'] = int(child.attributes.cy.value);

  drop_off_zone_data[child.attributes.id.value] = coords;
}

function get_polygon_data_svg (child) {
  var path_id = child.attributes.id.value;
  // console.log(path_id);
  var shape_data = [];
  var d = child.attributes.d.value;
  // console.log(d);
  // create a dummy object in the dom in order to read it with pathseg
  var dummy_path = document.createElementNS("http://www.w3.org/2000/svg", "path")
  dummy_path.setAttribute("d", d); //Set path's data
  // console.log(dummy_path);
  var pathSegList = dummy_path.pathSegList;
  // console.log(pathSegList);
  for (var j = 0; j < (pathSegList.numberOfItems -1); j++) {  // -1 last one is empty

    // console.log(Object.keys(pathSegList.getItem(j)));  // look inside
    // console.log(pathSegList.getItem(j).pathSegType);
    // console.log(pathSegList.getItem(j).pathSegTypeAsLetter);
    // console.log(pathSegList.getItem(j)._owningPathSegList);

    if (pathSegList.getItem(j).pathSegTypeAsLetter == "V") {  // vertical line https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d
      var x = int(pathSegList.getItem(j-1).x);
      var y = int(pathSegList.getItem(j).y);
    } else if (pathSegList.getItem(j).pathSegTypeAsLetter == "H") {  // horizontal line
      var x = int(pathSegList.getItem(j).x);
      var y = int(pathSegList.getItem(j-1).y);
    } else {
      var x = int(pathSegList.getItem(j).x);
      var y = int(pathSegList.getItem(j).y);
    }

    if (isNaN(x) | isNaN(y)) {
      var error_message = "The path " + path_id + " is missing pairwise coordinates. x: " + str(x) + ",y: " + str(y);
      logging.error(error_message);
      throw error_message;
    }

    shape_data.push({
      'x': x,
      'y': y
    })
  }
  polygon_data[path_id] = shape_data;
}


/**
write colors respective to x axis section to the drop off zone json.

check in which section the drop off zone x position falls.
*/
function allocate_drop_off_zone_to_section () {
  max_value = 50;
  for (let section in master_data) {
    for (let coord in drop_off_zone_data) {
      if (
        drop_off_zone_data[coord].x >= master_data[section].section_borders[0] &
        drop_off_zone_data[coord].x <= master_data[section].section_borders[1]
      ) {
        drop_off_zone_data[coord].color = master_data[section].color;
        drop_off_zone_data[coord].beat = master_data[section].beat + (Math.floor(Math.random() * max_value) + 1);  // from 1-30
      }
    }
  }
}

/**
checks the polygon data for string cloud and then creates either conventional polygon or cloud.
*/
function create_cloud_and_polygons () {
  for (let polygon_shape_data in polygon_data) {
    // check if "cloud" is in the path title
    var re_cloud = /cloud/;
    if (re_cloud.test(polygon_shape_data)) {
      cloud = new Cloud(polygon_data[polygon_shape_data], master_data);
    } else {
      polygon_shapes.push(
        new PolygonShape(polygon_data[polygon_shape_data], polygon_shape_data)
      );
    }
  }
}


function mouseClicked(event) {
  console.log("mouse is clicked.")
  deep_splashes.push(new Splash(deep_splash_frames, mouseX, CANVAS_HEIGHT, "black"));

  // print("number of bodies in this world: " + world.bodies.length);
}


// get the percent of number of inhabitants from total and convert to zones for cloud.
function calc_master_data_inhabitants() {
  // calculate total of inhabitants
  let total_inhabitants = 0
  for (let section in master_data ) {
    total_inhabitants += master_data[section]["number_of_inhabitants"];
  }
  // console.log(total_inhabitants);

  // calculate section borders for cloud
  let min_value = 0
  for (let section in master_data ) {
    let percent_inhabitants = master_data[section]["number_of_inhabitants"] / total_inhabitants
    // master_data[section]["percent_inhabitants"] = master_data[section]["number_of_inhabitants"] / total_inhabitants
    logging.debug("percent of inhabitants: " + percent_inhabitants);
    master_data[section]["min_value_absolute"] = min_value
    let max_value = Math.round(percent_inhabitants * CANVAS_WIDTH)
    master_data[section]["max_value_absolute"] = master_data[section]["min_value_absolute"] + max_value

    logging.debug(section + ": " + master_data[section]["min_value_absolute"] + ", " + master_data[section]["max_value_absolute"])
    master_data[section]["section_borders"] = [master_data[section]["min_value_absolute"], master_data[section]["max_value_absolute"]]

    min_value += max_value
  }
}


function read_table_infections() {
  // Basics
  // print(table);
  // print(table.getRowCount() + ' total rows in table');
  // print(table.getColumnCount() + ' total columns in table');
  // print(table.getColumn('Anzahl'));

  // for (let r = 0; r < table.getRowCount(); r++) {
  //   for (let c = 0; c < table.getColumnCount(); c++) {
  //     print(table.getString(r, c));
  //   }
  // }

  // several rows - but only last timestamp
  // let rows = table.matchRows(new RegExp('2020-09-01'), "Timestamp");
  // for (let row of rows) {
    //   if (row.obj['Bundesland'] == "Wien") {
      //     let vienna_infections = row.obj['Anzahl'];
      //     print(vienna_infections);
      //   }
      // }

  //Search using specified regex on a given column, return TableRow object
  if (SWITCH_REALTIME_DATA) {
    states = ["Wien", "Niederösterreich", "Oberösterreich"]

    // limits
    const INFECTIONS_START = 0;
    const INFECTIONS_STOP = 8000;

    const BEAT_START = 10;
    const BEAT_STOP = 500;

    for (let state of states) {
      let row = table_infections.matchRow(new RegExp(state), "Bundesland");
      logging.debug("Searched for " + state + " and found " + str(row.obj['Bundesland']) + " with " + str(row.obj['Anzahl']) + " infections.");

      //  write to master data
      for (let section in master_data ) {
        if (master_data[section]['data_label'] == state) {
          let infections = parseInt(row.obj['Anzahl']);
          // master_data[section]["number_of_infections"] = infections;
          logging.info("number of infections: " + infections);
          // mapping real numbers to beat - inverting the scale
          master_data[section]["beat"] = BEAT_STOP - Math.round(map(infections, INFECTIONS_START, INFECTIONS_STOP, BEAT_START, BEAT_STOP, true));
        }
      }
    }
  }
}


function read_table_beds() {

  if (SWITCH_REALTIME_DATA) {
    const BEDS_START = 1;
    const BEDS_STOP = 30;
    const ACTIVE_POLYGONS_START = 3;
    const ACTIVE_POLYGONS_STOP = 8;

    let percent_beds_intensive = 0;  // default value
    let today = new Date();

    let date_string = get_date_string(today);
    let row_current = table_beds_intensive.matchRow(new RegExp(date_string), "time");
    try {
      let percent_beds_intensive = row_current.obj['Belegung Intensivbetten in %'];
    }
    catch(err) {
      logging.debug("No data for today found for intensive beds. Getting data for yesterday.")

      let yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);
      let date_string = get_date_string(yesterday);

      let row_current = table_beds_intensive.matchRow(new RegExp(date_string), "time");
      // print(row_current)
      let percent_beds_intensive = row_current.obj['Belegung Intensivbetten in %'];
    }
    finally {
      logging.info("Searched for date " + date_string + " and found " + percent_beds_intensive + " percent of intensive beds used.");

      //  write to master data total
      master_data_total["active_polygons"] = Math.round(map(percent_beds_intensive, BEDS_START, BEDS_STOP, ACTIVE_POLYGONS_START, ACTIVE_POLYGONS_STOP, true));
    }
  }
}


function read_table_recovered() {

  if (SWITCH_REALTIME_DATA) {
    //Search using specified regex on a given column, return TableRow object
    states = ["Wien", "Niederösterreich", "Oberösterreich"]

    // limits
    const RECOVERED_START = 0;
    const RECOVERED_STOP = 0.01;

    const PUFF_START = 100;
    const PUFF_STOP = 1000;

    for (let state of states) {
      let row = table_recovered.matchRow(new RegExp(state), "Bundesland");
      logging.debug("Searched for " + state + " and found " + str(row.obj['Bundesland']) + " with " + str(row.obj['Genesen']) + " recovered.");


      // write to master data
      for (let section in master_data ) {
        if (master_data[section]['data_label'] == state) {
          let recovered = parseInt(row.obj['Genesen']);
          master_data[section]["number_of_recovered"] = recovered;
        }
      }
    }

    let total_recovered = 0;
    let total_inhabitants = 0;
    for (let section in master_data ) {
      total_recovered += master_data[section]["number_of_recovered"]
      total_inhabitants += master_data[section]["number_of_inhabitants"]
    }
    // master_data_total["total_recovered"] = total_recovered;
    // master_data_total["total_inhabitants"] = total_inhabitants;
    logging.info("total recovered: " + total_recovered);
    logging.info("total inhabitants: " + total_inhabitants);

    let recovered_percentage;
    recovered_percentage = total_recovered/ total_inhabitants
    master_data_total["polygon_puff"] = PUFF_STOP - Math.round(map(recovered_percentage, RECOVERED_START, RECOVERED_STOP, PUFF_START, PUFF_STOP, true));
    logging.debug("puff_polygon: " + master_data_total["polygon_puff"]);
  }
}

// get a string of a date in form: 01.02.2020
function get_date_string(date) {
  var dd = String(date.getDate()).padStart(2, '0');
  var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = date.getFullYear();
  date_string = dd + '.' + mm + '.' + yyyy;

  return date_string;
}
